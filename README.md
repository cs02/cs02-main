Avaloq Search Engine
====================


Running the Search Engine on windows
==============================


Using anaconda or a terminal of your choice: 

Download Elastic Search from https://www.elastic.co/downloads/elasticsearch by selecting ‘Windows’ in the downloads section. 

Git Clone Repository from https://gitlab.com/cs02/cs02-main.git into desired folder.

Navigate to the repository. 

Set up virtual environment. If not already downloaded, download virtualenv using: "pip install virtualenv” into the terminal. Once downloaded then use  'virtualenv ‘name of choice'' to create virtual environment. 

Once virtual environment is set up, run it by entering:
    
    $ source venv\bin\activate

Navigate to Django folder and run:

    $ pip install -r requirements.txt

Start ElasticSearch by running the windows batch file located in elasticsearch-7.6.0\bin.


Navigate to `\django\avaloq_django_project` and run the follow commands into the terminal: 

	$ python manage.py makemigrations
	$ python manage.py migrate
	$ python manage.py generate_data 10 
	$ python manage.py index


Finally the database has been filled and searching is ready. 

Running `python manage.py runserver` will activate the server. The website will be at the url that appears in the terminal.
  
 
 
Running the Search Engine on Linux
=======================================

The Linux instructions work on ubuntu. We have no idea if they will for other distros.

Git clone and navigate to the location of the repository 
 
Set up virtual environment. If not already downloaded, download virtualenv using: "pip install virtualenv” into the terminal. Once downloaded then use  'virtualenv ‘name of choice'' to create virtual environment. \\
 
Once virtual environment is set up, run it by navigating to the correct repository and entering:

    $ cd venv/bin
    $ source activate
    

Navigate to Django folder and run:

    $ pip install -r requirements.txt


Setting up ElasticSearch for Linux
---------------------
This project uses ElasticSearch as an indexing engine with ElasticSearchHQ as
a manager for the engine. To set up ElasticSearch and ESHQ to work with the
Django web application, we use Docker.

https://docs.docker.com/install/linux/docker-ce/ubuntu/

Once you have installed Docker, run:

    $ docker network create avaloq-elasticsearch

This will create an internal bridged network so ESHQ and ES can communicate.

To run ElasticSearch via Docker, run:

    $ docker run -d --name avaloq-elasticsearch --net avaloq-elasticsearch -p 9200:9200 -p 9300:9300 -e "discovery.type=single-node" elasticsearch:7.5.2

This will launch a container on your local machine with ElasticSearch installed. To test this works,
navigate to `http://localhost:9200` in a web browser. You should see something similar to the
following JSON output:

    {
        name: "bdfbf390090f",
        cluster_name: "docker-cluster",
        cluster_uuid: "M1U9gkMoSyKXsC6saKPWlQ",
        version: {
            number: "7.5.2",
            build_flavor: "default",
            build_type: "docker",
            build_hash: "8bec50e1e0ad29dad5653712cf3bb580cd1afcdf",
            build_date: "2020-01-15T12:11:52.313576Z",
            build_snapshot: false,
            lucene_version: "8.3.0",
            minimum_wire_compatibility_version: "6.8.0",
            minimum_index_compatibility_version: "6.0.0-beta1"
        },
        tagline: "You Know, for Search"
    }

You can use `docker -a ps` to see all containers you have created. Use `docker kill`
to stop a running container or `docker rm` to remove it altogether.


Accessing Search Engine for Linux
-----------------------------

Navigate to `/django/avaloq_django_project` and run the following commands in the terminal: 

	$ python manage.py makemigrations
	$ python manage.py migrate
	$ python manage.py generate_data 10 
	$ python manage.py index


Finally the database has been filled and searching is ready. 

Running `python manage.py runserver` will activate the server. The website will be at the url that appears in the terminal.
  
  
Using the Security Model
-------------------------

The security model is currently in a separate branch as its front end runs significantly slower.

To use it you need to checkout `feature/91-attempt-second-security-model` and run

    $ python manage.py makemigrations
	$ python manage.py migrate
	$ python manage.py generate_data 10 
	$ python manage.py index
 
 again as there is a change to one of the models.
 
 Now search for a portfolio. Phrases like 'one-to-one', 'schema' or 'vectors' should return some that you can search for.
 
 Copy the name of the first person in the results.
 
 Open views.py in `\django\avaloq_django_project\avaloq_search_app`. 
 
 Paste the name into the variable on line 29. The line should then look like `person = "<the copied name>"`.
 
 Save views.py and reload the web page.
 
 The results should now be restricted.
 
 It is also possible to choose from a few names in the drawer in the top left however you won't know what they should be able to access when you search.
 
 Known Errors
 ------------
 Sometimes the `generate_data` command may give an error that a company doesn't exist. This seems to occur after the generation commands have been used many times. 
 
 To fix this error delete the file `django/avaloq_django_project/db.sqlite3` and re-run all the commands used above to create the database.
 
 Extra Docker Information 
 -------------------------

To get the private IP address of the ES container over the bridged network, run:

    $ docker inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' avaloq-elasticsearch
    172.18.0.2

Then, the IP address for the container is `172.18.0.2`. Now, create and run the
container with ElasticSearchHQ:

    $ docker run -d --name avaloq-elasticsearch-hq --net avaloq-elasticsearch -p 5000:5000 elastichq/elasticsearch-hq

Now, navigate to `http://localhost:5000`. You should see the ElasticSearch HQ panel. Enter
`http://172.18.0.2:9200` under *Connect to ElasticSearch*, where `172.18.0.2` is the IP address
you obtained from the command above.

You should now be able to log into ESHQ and manage your local ElasticSearch environment. To purge
your ES container, just run `docker rm avaloq-elasticsearch` and re-do the appropriate `docker run`
command.
