from django.apps import AppConfig


class AvaloqSearchAppConfig(AppConfig):
    name = 'avaloq_search_app'
