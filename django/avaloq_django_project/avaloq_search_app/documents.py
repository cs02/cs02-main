from elasticsearch_dsl import Document, Index
from django.conf import settings
from .models import Account, AssetType, AssetPriceHistory, Transaction, Person, Company, Portfolio


index = Index(settings.ES_INDEX)
index.settings(number_of_shards=1, number_of_replicas=0)

class AllFields(Document):
    class Meta:
        fields = [
            'asset',
            'amount',
            'name',
            'code',
            'date',
            'rate',
            'time',
            'dob',
        ]

class AccountDocument(Document):
    class Meta:
        model = Account
        fields = [
            'asset',
            'amount',
            'portfolio'
        ]

class AssetTypeDocument(Document):
    class Meta:
        model = AssetType
        fields = [
            'name',
            'code',
        ]

class AssetPriceHistoryDocument(Document):
    class Meta:
        model = AssetPriceHistory
        fields = [
            'date',
            'rate',
            'asset_from',
            'asset_to'
        ]

class PortfolioDocument(Document):
    class Meta:
        model = Portfolio
        fields = [
            'name',
        ]

class TransactionDocument(Document):
    class Meta:
        model = Transaction
        fields = [
            'time',
            'account',
            'amount',
        ]

class PersonDocument(Document):
    class Meta:
        model = Person
        fields = [
            'name',
            'dob',
            'portfolios',
        ]

class CompanyDocument(Document):
    class Meta:
        model = Company
        fields = [
            'name',
            'address',
            'phone_no',
            'people'
        ]