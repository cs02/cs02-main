
myFunction({{ hits| safe}});
function myFunction(data) {
    var total = Object.keys(data).length;
    var badge = document.createElement('div');
    badge.className = 'badge';
    badge.innerHTML =
        '<div> Total Results: ' + total + '</div>'
    console.log(badge.innerHTML.toString())
    //I gave the div the same ID's as the keys in the object for ease
    document.getElementById("cool").appendChild(badge);

    for (var key in data) {
        console.log(data[key.toString()])

        if (data[key.toString()]._id.toString().includes("account")) {
            MakeAccount(key);
        }

        if (data[key.toString()]._id.toString().includes("asset")) {
            MakeAssetType(key);
        }

        if (data[key.toString()]._id.toString().includes("portfolio")) {
            MakePortfolio(key);
        }

        if (data[key.toString()]._id.toString().includes("transaction")) {
            MakeTransaction(key);
        }

        if (data[key.toString()]._id.toString().includes("person")) {
            MakePerson(key);
        }

        if (data[key.toString()]._id.toString().includes("company")) {
            MakeCompany(key);
        }

        if (data[key.toString()]._id.toString().includes("history")) {
            MakeAssetPriceHistory(key);
        }
    }
    function MakeAccount(key) {

        var assetname = data[key.toString()]._source.asset.name;
        var amount = data[key.toString()]._source.amount;
        var portfolio = data[key.toString()]._source.portfolio.name;

        var badge = document.createElement('div');
        badge.className = 'badge';
        badge.innerHTML =
            '<div> Account: ' + assetname + " Amount: " + amount + " Portfolio: " + portfolio + '</div>'
        console.log(badge.innerHTML.toString())
        //I gave the div the same ID's as the keys in the object for ease
        document.getElementById("cool").appendChild(badge);
    }

    function MakeAssetType(key) {

        var name = data[key.toString()]._source.name;
        var code = data[key.toString()]._source.code;

        var badge = document.createElement('div');
        badge.className = 'badge';
        badge.innerHTML =
            '<div> AssetType: ' + name + " Code: " + code + '</div>'
        console.log(badge.innerHTML.toString())
        //I gave the div the same ID's as the keys in the object for ease
        document.getElementById("cool").appendChild(badge);
    }

    function MakePortfolio(key) {

        var name = data[key.toString()]._source.name;

        var badge = document.createElement('div');
        badge.className = 'badge';
        badge.innerHTML =
            '<div> Portfolio: ' + name + '</div>'
        console.log(badge.innerHTML.toString())
        //I gave the div the same ID's as the keys in the object for ease
        document.getElementById("cool").appendChild(badge);
    }

    function MakeTransaction(key) {

        var time = data[key.toString()]._source.time;
        var account = data[key.toString()]._source.account.asset.name;
        var amount = data[key.toString()]._source.account.amount;

        var badge = document.createElement('div');
        badge.className = 'badge';
        badge.innerHTML =
            '<div> Transaction: ' + time.toLocaleString() + " Account: " + account + " Amount: " + amount + '</div>'
        console.log(badge.innerHTML.toString())
        //I gave the div the same ID's as the keys in the object for ease
        document.getElementById("cool").appendChild(badge);
    }

    function MakePerson(key) {

        var name = data[key.toString()]._source.time;
        var dob = data[key.toString()]._source.account.asset.name
        var portfolios = data[key.toString()]._source.account.amount;

        var badge = document.createElement('div');
        badge.className = 'badge';
        badge.innerHTML =
            '<div> Person: ' + name + " DOB: " + dob + " Portfolio 1: " + portfolios + '</div>'
        console.log(badge.innerHTML.toString())
        //I gave the div the same ID's as the keys in the object for ease
        document.getElementById("cool").appendChild(badge);
    }

    function MakeCompany(key) {

        var name = data[key.toString()]._source.time;
        var address = data[key.toString()]._source.address
        var phone_no = data[key.toString()]._source.phone_no;
        var people = data[key.toString()]._source.people;

        var badge = document.createElement('div');
        badge.className = 'badge';
        badge.innerHTML =
            '<div> Company: ' + name + " Address: " + address + " Phone Number: " + phone_no + " Person 1: " + people + '</div>'
        console.log(badge.innerHTML.toString())
        //I gave the div the same ID's as the keys in the object for ease
        document.getElementById("cool").appendChild(badge);
    }
    function MakeAssetPriceHistory(key) {

        var date = data[key.toString()]._source.time;
        var rate = data[key.toString()]._source.account.asset.name
        var asset_from = data[key.toString()]._source.account.amount;
        var asset_to = data[key.toString()]._source.account.amount;

        var badge = document.createElement('div');
        badge.className = 'badge';
        badge.innerHTML =
            '<div> Price History: ' + date + " Rate: " + rate + " Asset_from: " + asset_from + " Asset_to: " + asset_to + '</div>'
        console.log(badge.innerHTML.toString())
        //I gave the div the same ID's as the keys in the object for ease
        document.getElementById("cool").appendChild(badge);
    }



}
