from django.shortcuts import render, redirect, reverse
from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.conf import settings
from django.views import View
from operator import itemgetter
from .documents import AllFields

import elasticsearch
from elasticsearch_dsl import Search, Index, Q
from elasticsearch_dsl.query import MultiMatch

class LandingPageView(View):
    
    def get(self, request):
        q = request.GET.get('q')

        if not q:
            context_dict = {"boldmessage": "AVALOQ SEARCH ENGINE"}
            return render(request, "avaloq_search_app/search_page.html", context=context_dict)
        else:
            es = elasticsearch.Elasticsearch(**settings.ES_CONNECTIONS)

            r = es.search(index="avaloq-search", q=q, size=1000)
            r=r["hits"]
            r["hits"] = LandingPageView.mult_score(r["hits"], q)       

            return render(request, "avaloq_search_app/search_page.html", context=r)
    
    # Applies a boost value to each result
    def mult_score(es, query):
        for q in es:
            if('portfolio' in q["_id"] and q["_source"]["name"] != query):
                q["_score"] = q["_score"] * (q["_source"]["boost"]*0.8)
            else:
                q["_score"] = q["_score"] * q["_source"]["boost"]
        return sorted(es, key=itemgetter("_score"), reverse=True)