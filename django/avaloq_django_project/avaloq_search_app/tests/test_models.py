from django.test import TestCase
from django.db import transaction
from avaloq_search_app.models import AssetType, AssetPriceHistory, Portfolio, Account, Transaction, Person, Company


class Test_Models(TestCase):

    def setUp(self):

        # Create some AssetType objects
        self.asset_type1 = AssetType.objects.create(name='United States Dollar', code='USD')
        self.asset_type2 = AssetType.objects.create(name='Pound sterling', code='GBP')

        # Create AssetPriceHistory object
        self.asset_price_history1 = AssetPriceHistory.objects.create(
            date='2020-01-01', rate=0.77, asset_from=self.asset_type1, asset_to=self.asset_type2)
        self.asset_price_history2 = AssetPriceHistory.objects.create(
            date='2020-01-01', rate=1.30, asset_from=self.asset_type2, asset_to=self.asset_type1)

        # Create Portfolio object
        self.portfolio1 = Portfolio.objects.create(name='sticky partnerships')
        self.portfolio2 = Portfolio.objects.create(name='sticky')



        # Create Person object
        self.person1 = Person.objects.create(name='John', dob='1983-01-01')
        self.person1.save()
        self.person1.portfolios.add(self.portfolio1)

        self.person2 = Person.objects.create(name='Eian', dob='1983-02-01')
        self.person2.save()
        self.person2.portfolios.add(self.portfolio2, self.portfolio1)

        # Create Account object
        self.account = Account.objects.create(amount='2000', asset=self.asset_type1, portfolio=self.portfolio1)

        # Create Transaction object
        self.transaction = Transaction.objects.create(time='2020-01-01 01:01:01Z', amount=45, account=self.account)

        # Create Company object
        self.company1 = Company(name='Avaloq', address='G1 3PQ', phone_no='12345678')
        self.company1.save()
        self.company1.people.add(self.person1)

        self.company2 = Company(name='Barclays', address='G2 8QH', phone_no='78945612')
        self.company2.save()
        self.company2.people.add(self.person2, self.person1)
    #
    # Tests of AssetType
    #

    def test_asset_type_name_label(self):
        field_label = self.asset_type1._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'name')

    def test_asset_type_code_label(self):
        field_label = self.asset_type1._meta.get_field('code').verbose_name
        self.assertEquals(field_label, 'code')

    def test_asset_type_name_max_value(self):
        max_length = self.asset_type1._meta.get_field('name').max_length
        self.assertEquals(max_length, 256)

    def test_asset_type_search(self):
        self.assertEquals(self.asset_type1.to_search(), ({'name': 'United States Dollar', 'code': 'USD'}))

    #
    # Tests of AssetPriceHistory
    # Some very basic tests

    def test_asset_price_history_asset_from_label(self):
        field_label = self.asset_price_history1._meta.get_field('asset_from').verbose_name
        self.assertEquals(field_label, 'asset from')

    def test_asset_price_history_asset_to_label(self):
        field_label = self.asset_price_history1._meta.get_field('asset_to').verbose_name
        self.assertEquals(field_label, 'asset to')

    def test_asset_price_history_date_label(self):
        field_label = self.asset_price_history1._meta.get_field('date').verbose_name
        self.assertEquals(field_label, 'date')

    def test_asset_price_history_rate_label(self):
        field_label = self.asset_price_history1._meta.get_field('rate').verbose_name
        self.assertEquals(field_label, 'rate')

    def test_asset_price_history_search(self):
        self.assertEquals(self.asset_price_history1.to_search(),
                          ({'date': '2020-01-01', 'rate': 0.77,
                            'asset_from': {'name': 'United States Dollar', 'code': 'USD'},
                            'asset_to': {'name': 'Pound sterling', 'code': 'GBP'}}))

    # Tests for one to many relationships of Asset Type and Asset Price History
    # AssetType objects have access to their related AssetPriceHistory objects.
    def test_get(self):
        asset_type1 = self.asset_price_history1.asset_from
        self.assertEqual(asset_type1, self.asset_type1)
        self.assertEqual((asset_type1.name, self.asset_type1.code), ('United States Dollar', 'USD'))

    def test_create(self):
        h1 = AssetPriceHistory(date='2020-01-01', rate=1.30, asset_from=self.asset_type2,
                               asset_to=self.asset_type1)
        h1.save()
        self.assertEqual(h1.asset_from, self.asset_type2)
        self.assertEqual(h1.asset_to, self.asset_type1)

        # Similarly, the reporter ID can be a string.
        h2 = AssetPriceHistory(date='2020-01-01', rate=0.77, asset_from=self.asset_type1,
                               asset_to=self.asset_type2)
        h2.save()
        self.assertEqual(repr(h2.asset_from), "<AssetType: United States Dollar (USD)>")
        self.assertEqual(repr(h2.asset_to), "<AssetType: Pound sterling (GBP)>")

    def test_add(self):
        # Create an AssetPriceHistory via the AssetType object.
        asset_price_history_set = set()
        h1 = AssetPriceHistory.objects.create(
            date='2020-01-01', rate=0.77, asset_from=self.asset_type1, asset_to=self.asset_type2)

        asset_price_history_set.add(h1)
        self.assertEqual(repr(h1),
                         "<AssetPriceHistory: United States Dollar (USD) -> Pound sterling (GBP): 0.77 (2020-01-01)>")
        self.assertEqual(repr(h1.asset_from), "<AssetType: United States Dollar (USD)>")

            # Create a new AssetPriceHistory, and add it to the AssetPriceHistory set.
        h2 = AssetPriceHistory.objects.create(date='2020-01-01', rate=1.30, asset_from=self.asset_type2,
                                              asset_to=self.asset_type1)
        asset_price_history_set.add(h2)
        self.assertQuerysetEqual(
            asset_price_history_set,
            ["<AssetPriceHistory: United States Dollar (USD) -> Pound sterling (GBP): 0.77 (2020-01-01)>",
             "<AssetPriceHistory: Pound sterling (GBP) -> United States Dollar (USD): 1.3 (2020-01-01)>"]
        )


    # Tests of many to many relationships of Person and Portfolio
    def test_many_to_many_add(self):
        # Create a portfolio.
        self.p3 = Person.objects.create(name='Django', dob='1983-01-01')
        self.p3.save()

        # Associate the Person with a Portfolio.
        self.p3.portfolios.add(self.portfolio1)
        self.assertQuerysetEqual(self.p3.portfolios.all(), ['<Portfolio: sticky partnerships>'])
        # Create another Article, and set it to appear in both portfolios.
        self.p4 = Person.objects.create(name='Ean', dob='1983-01-02')
        self.p4.save()
        self.p4.portfolios.add(self.portfolio1, self.portfolio2)
        # Adding a second time is OK
        self.p4.portfolios.add(self.portfolio1)

        self.assertQuerysetEqual(
            self.p4.portfolios.all(),
            [
                '<Portfolio: sticky partnerships>',
                '<Portfolio: sticky>',
            ], ordered=False
        )

        # Adding an object of the wrong type raises TypeError
        try:
            with transaction.atomic():
                self.p4.portfolios.add(self.p3)
        except TypeError as e:
            self.assertTrue("'Portfolio' instance expected, got <Person: Django>" in str(e))

        # Add a Portfolio directly via portfolios.add by using keyword arguments.
        self.p4.portfolios.create(name='rbs')
        self.assertQuerysetEqual(
            self.p4.portfolios.all(),
            [
                '<Portfolio: sticky partnerships>',
                '<Portfolio: sticky>',
                '<Portfolio: rbs>',
            ], ordered=False
        )
        
    def test_create(self):
        asset_type3 = AssetType.objects.create(name='Euro', code='EUR')
        asset_type3.save()
        self.assertEqual(repr(asset_type3), "<AssetType: Euro (EUR)>")

        h1 = AssetPriceHistory.objects.create(
            date='2020-01-01', rate=0.77, asset_from=self.asset_type1, asset_to=self.asset_type2)
        h1.save()
        self.assertEqual(repr(h1),
                        "<AssetPriceHistory: United States Dollar (USD) -> Pound sterling (GBP): 0.77 (2020-01-01)>")

        p3 = Portfolio.objects.create(name='RBS')
        p3.save()
        self.assertEqual(repr(p3), '<Portfolio: RBS>')

        a3 = Account.objects.create(amount='2000', asset=self.asset_type1, portfolio=self.portfolio1)
        a3.save()

        self.assertEqual(a3.amount, '2000')
        self.assertEqual(repr(a3.asset), '<AssetType: United States Dollar (USD)>')
        self.assertEqual(repr(a3.portfolio), '<Portfolio: sticky partnerships>')

        t3 = Transaction.objects.create(time='2020-01-01 01:01:01Z', amount=45, account=self.account)
        t3.save()
        self.assertEqual(repr(t3), '<Transaction: 45 United States Dollar (USD)>')

        c3 = Company(name='Avaloq', address='G1 3PQ', phone_no='12345678')
        c3.save()
        self.assertEqual(repr(c3), '<Company: Avaloq>')

        person3 = Person.objects.create(name='John', dob='1983-01-01')
        person3.save()
        self.assertEqual(repr(person3), '<Person: John>')
        
    # Tests of Portfolio
    #
    def test_portfolio_name_label(self):
        field_label = self.portfolio1._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'name')

    def test_portfolio_search(self):
        self.assertEquals(self.portfolio1.to_search(), ({'name': 'sticky partnerships'}))

    def test_portfolio_name_max_value(self):
        max_length = self.portfolio1._meta.get_field('name').max_length
        self.assertEquals(max_length, 256)

    # Tests of Account and many to one relationships

    # AssetType objects have access to their related AssetPriceHistory objects.
    def test_get(self):
        asset_type1 = self.asset_price_history1.asset_from
        self.assertEqual(asset_type1, self.asset_type1)
        self.assertEqual((asset_type1.name, self.asset_type1.code), ('United States Dollar', 'USD'))

    # Test of Account object
    def test_account_amount_label(self):
        field_label = self.account._meta.get_field('amount').verbose_name
        self.assertEquals(field_label, 'amount')

    def test_account_search(self):
        self.assertEqual(self.account.to_search(), ({'asset': {'name': 'United States Dollar', 'code': 'USD'},
                                                     'amount': '2000',
                                                     'portfolio': {'name': 'sticky partnerships'}}))

    # Test of Transaction object
    def test_transaction_time_label(self):
        field_label = self.transaction._meta.get_field('time').verbose_name
        self.assertEquals(field_label, 'time')

    def test_transaction_search(self):
        self.assertEquals(self.transaction.to_search(),
                          ({'time': '2020-01-01 01:01:01Z',
                            'account': {'asset': {'name': 'United States Dollar', 'code': 'USD'}, 'amount': '2000',
                                        'portfolio': {'name': 'sticky partnerships'}},
                              'amount': 45}))

    # Test of Person
    def test_person_name_label(self):
        field_label = self.person1._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'name')

    # # Test of Company
    def test_Company_name_label(self):
        field_label = self.company1._meta.get_field('name').verbose_name
        self.assertEquals(field_label, 'name')

    def test_Company_name_max_value(self):
        max_length = self.company1._meta.get_field('name').max_length
        self.assertEquals(max_length, 256)
