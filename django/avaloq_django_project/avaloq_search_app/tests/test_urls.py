from django.test import SimpleTestCase
from django.urls import reverse, resolve
from avaloq_search_app.views import LandingPageView

class TestUrls(SimpleTestCase):

    def test_landing_page_is_resolved(self):
        url = reverse('avaloq_search_app:landing_page')
        self.assertEquals(resolve(url).func.__name__, LandingPageView.__name__)
