from django.test import TestCase, Client
from django.urls import reverse


class TestViews(TestCase):

    def setUp(self):
        self.client = Client()
        self.landing_page_url = reverse('avaloq_search_app:landing_page')

    def test_landing_page_view_GET(self):
        response = self.client.get(self.landing_page_url)
        self.assertEquals(response.status_code, 200)
        self.assertTemplateUsed(response, 'avaloq_search_app/search_page.html')
