from django.urls import path
from avaloq_search_app import views

app_name = 'avaloq_search_app'

urlpatterns = [
    path('', views.LandingPageView.as_view(), name='landing_page'),
]
