from django.db import models

class AssetType(models.Model):
    # The name of the asset (e.g. United States Dollar)
    name = models.CharField(max_length=256)

    # The code of the asset (e.g. USD)
    code = models.CharField(max_length=256, primary_key=True)

    def to_search(self):
        return {
            'name': self.name,
            'code': self.code
        }
    
    def __str__(self):
        return f"{self.name} ({self.code})"

class AssetPriceHistory(models.Model):
    asset_from = models.ForeignKey(AssetType, on_delete=models.CASCADE, related_name="asset_from")
    asset_to = models.ForeignKey(AssetType, on_delete=models.CASCADE, related_name="asset_to")

    date = models.DateField()
    rate = models.DecimalField(decimal_places = 9, max_digits=20)

    def to_search(self):
        data = {
            'date': self.date,
            'rate': self.rate,
            'asset_from': self.asset_from.to_search(),
            'asset_to': self.asset_to.to_search()
        }

        return data
    
    def __str__(self):
        return f"{self.asset_from} -> {self.asset_to}: {self.rate} ({self.date})"
    
class Portfolio(models.Model):
    name = models.CharField(max_length=256)

    def to_search(self):
        data = {
            'name': self.name
        }

        return data
    
    def __str__(self):
        return self.name

class Account(models.Model):
    asset = models.ForeignKey(AssetType, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)
    portfolio = models.ForeignKey(Portfolio, on_delete=models.CASCADE)

    def to_search(self):
        data = {
            'asset': self.asset.to_search(),
            'amount': self.amount,
            'portfolio': self.portfolio.to_search()
        }

        return data
    
    def __str__(self):
        return f"{self.amount:,} of {self.asset} for {self.portfolio}"

class Transaction(models.Model):
    time = models.DateTimeField()
    account = models.ForeignKey(Account, on_delete=models.CASCADE)
    amount = models.IntegerField(default=0)

    def to_search(self):
        data = {
            'time': self.time,
            'account': self.account.to_search(),
            'amount': self.amount,
        }

        return data
    
    def __str__(self):
        return f"{self.amount} {self.account.asset}"

class Person(models.Model):
    name = models.CharField(max_length=256)
    dob = models.DateField()
    portfolios = models.ManyToManyField(Portfolio)
    
    def to_search(self):
        data = {
            'name': self.name,
            'dob': self.dob,
            'portfolios': [i.to_search() for i in self.portfolios.all()]

        }
        return data

    def __str__(self):
        return self.name

class Company(models.Model):
    name = models.CharField(max_length=256, primary_key=True)
    address = models.CharField(max_length=256)
    phone_no = models.CharField(max_length=256)
    people = models.ManyToManyField(Person)
    
    def to_search(self):
        data = {
            'name': self.name,
            'address': self.address,
            'phone_no': self.phone_no,
            'people': [i.to_search() for i in self.people.all()]
        }
        return data

    def __str__(self):
        return self.name