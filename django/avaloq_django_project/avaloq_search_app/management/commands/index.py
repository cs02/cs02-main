from django.core.management.base import BaseCommand, CommandError
from django.conf import settings

from elasticsearch import Elasticsearch
from elasticsearch.helpers import bulk

from avaloq_search_app.models import AssetType, AssetPriceHistory, Account, Transaction, Portfolio, Person, Company

MODELS = [AssetType, AssetPriceHistory, Account, Transaction, Portfolio, Person, Company]

class Command(BaseCommand):
    def handle(self, *args, **kwargs):
        self.es = Elasticsearch(**settings.ES_CONNECTIONS)

        for m in MODELS:
            self.verbose_run(m)
    
    def generate_document(self, model):
        objects = model.objects.all()

        index = model._meta.verbose_name.split()
        index = "_".join(index)

        for o in objects:
            # Each object given a boost value when indexed
            boost_amount = 1
            if(isinstance(o, Portfolio)):
                boost_amount = 1.55
            elif(isinstance(o, Account)):
                boost_amount = 1.4
            elif(isinstance(o, AssetType)):
                boost_amount = 1.35
            elif(isinstance(o, Transaction)):
                boost_amount = 1.3
            elif(isinstance(o, Person)):
                boost_amount = 1.5
            elif(isinstance(o, Company)):
                boost_amount = 1.45
            elif(isinstance(o, AssetPriceHistory)):
                boost_amount = 1.3
            
            to_add = {
                "_index": "avaloq-search",
                "_type": "document",
                "_id": f'{index}-{o.pk}',
                "boost": boost_amount,
                **o.to_search()
            }

            yield to_add

    def verbose_run(self, model, report_every=100):
        name = model._meta.verbose_name
        print(f'Indexing {name}')

        bulk(self.es, self.generate_document(model))
