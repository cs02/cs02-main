import random
import itertools
import datetime

from django.core.management.base import BaseCommand, CommandError
from avaloq_search_app.models import AssetType, AssetPriceHistory, Account, Transaction, Portfolio, Company, Person

from faker import Faker

class Command(BaseCommand):
    help = "Creates fake data for the database"
    LOCALES = ["ar_EG", "ar_PS", "ar_SA", "bg_BG", "bs_BA", "cs_CZ", "de_DE", "dk_DK", "el_GR", "en_AU", "en_CA", "en_GB", "en_NZ", "en_US", "es_ES", "es_MX", "et_EE", "fa_IR", "fi_FI", "fr_FR", "hi_IN", "hr_HR", "hu_HU", "hy_AM", "it_IT", "ja_JP", "ka_GE", "ko_KR", "lt_LT", "lv_LV", "ne_NP", "nl_NL", "no_NO", "pl_PL", "pt_BR", "pt_PT", "ro_RO", "ru_RU", "sl_SI", "sv_SE", "tr_TR", "uk_UA", "zh_CN", "zh_TW"]

    def add_arguments(self, parser):
        parser.add_argument('total', type=int, help='Order of magnitude of data to be created')

    def handle(self, *args, **kwargs):
        self.companies = set()
        self.people = set()
        self.assets = set()
        self.portfolios = set()
        self.accounts = set()

        total = kwargs['total']
        n_people = total * random.randrange(5, 15)
        n_assets = total * random.randrange(1, 3)
        n_portfolios = n_people * 2

        self.stdout.write(f"Creating {total} companies...")
        for _ in range(total):
            self.companies.add(self.create_company())

        self.stdout.write(f"Creating {n_people} people...")
        for _ in range(n_people):
            self.people.add(self.create_people())

        self.stdout.write(f"Creating {n_assets} currency assets...")
        for _ in range(n_assets):
            self.assets.add(self.create_currency())

        self.stdout.write(f"Creating {n_assets} stock assets...")
        for _ in range(n_assets):
            self.assets.add(self.create_stock())

        self.stdout.write("Removing companies that have no people...")
        self.remove_empty_companies()

        self.stdout.write(f"Creating {n_assets} stock assets...")
        for _ in range(n_assets):
            self.assets.add(self.create_stock())

        self.stdout.write(f"Creating a portfolio for each person...")
        for p in self.people:
            portfolio = self.create_portfolio()
            p.portfolios.add(portfolio)
            self.portfolios.add(portfolio)

        self.stdout.write(f"Creating some portfolios for sharing (2 people)...")
        for i, p in enumerate(itertools.product(self.people, repeat=2)):
            if i >= n_people:
                break

            portfolio = self.create_portfolio()

            p1, p2 = p
            p1.portfolios.add(portfolio)
            p2.portfolios.add(portfolio)

            self.portfolios.add(portfolio)
        
        self.stdout.write(f"Creating accounts for portfolios...")
        for p in self.portfolios:
            for _ in range(random.randrange(1, 3)):
                self.accounts.add(self.create_account(p))

        self.stdout.write(f"Creating asset price histories for all currencies...")
        for a in itertools.product(self.assets, repeat=2):
            self.generate_asset_price_history(a)
        
        self.stdout.write(f"Creating transaction histories for accounts...")
        for a in self.accounts:
            self.generate_transaction_history(a)



    def create_company(self):
        fake = Faker(random.choice(Command.LOCALES))

        to_create = {
            "name": fake.company(),
            "address": fake.address(),
            "phone_no": fake.phone_number(),
        }

        o, created = Company.objects.get_or_create(**to_create)

        return o

    def create_people(self):
        fake = Faker(random.choice(Command.LOCALES))

        p = fake.simple_profile(sex=None)

        to_create = {
            "name": p["name"],
            "dob": p["birthdate"]
        }

        # Create person
        o, created = Person.objects.get_or_create(**to_create)

        # Get a random sample of companies and add the person to that company.
        company_sample = random.sample(self.companies, random.randrange(1, 3))
        for c in self.companies:
            c.people.add(o)

        return o

    def create_currency(self):
        fake = Faker()

        code, name = fake.currency()

        to_create = {
            "name": name,
            "code": code
        }

        o, created = AssetType.objects.get_or_create(**to_create)

        return o

    def create_stock(self):
        locale = random.choice(Command.LOCALES)
        fake = Faker(locale)

        to_create = {
            "name": f"{fake.company()} stock",
            "code": f"STOCK-{locale}-{random.randrange(0, 10e6)}"
        }

        o, created = AssetType.objects.get_or_create(**to_create)

        return o

    def remove_empty_companies(self):
        companies = Company.objects.filter(people=None)
        print(f"Removing {companies.count()} companies that have no associated people...")

        for c in companies:
            self.companies.remove(c)

        companies.delete()

    def create_portfolio(self):
        fake = Faker()
        name = Faker().bs().split()
        name = " ".join(name[1:])

        o, created = Portfolio.objects.get_or_create(name=name)

        return o

    def create_account(self, portfolio):
        to_create = {
            "asset": random.choice(tuple(self.assets)),
            "amount": random.randrange(0, 10000000),
            "portfolio": portfolio
        }

        o, created = Account.objects.get_or_create(**to_create)

        return o
    
    def generate_asset_price_history(self, assets):
        a1, a2 = assets

        if "stock (STOCK" in str(a1) or "stock (STOCK" in str(a2):
            return
        
        if str(a1) == str(a2):
            return
        
        rate = random.randrange(10, 50000)
        
        to_create = {
            'date': None,
            'rate': None,
            'asset_from': a1,
            'asset_to': a2
        }
        
        for d in (datetime.date.today() - datetime.timedelta(n) for n in range(30)):
            to_create['date'] = d
            to_create['rate'] = rate + random.randrange(-2, +2)
            rate = to_create['rate']

            o, created = AssetPriceHistory.objects.get_or_create(**to_create)
    
    def generate_transaction_history(self, source):
        destinations = random.sample(self.accounts, random.randrange(1, 4))

        for destination in destinations:
            for d in (datetime.datetime.now(tz=datetime.timezone.utc) - datetime.timedelta(n) for n in random.sample(range(360), 3)):
                amount = random.randrange(0, 10000) * random.choice((-1, 1))

                to_create = {
                    'time': d,
                    'account': source,
                    'amount': amount
                }

                o, created = Transaction.objects.get_or_create(**to_create)

                to_create["account"] = destination
                to_create["amount"] = amount * -1

                o, created = Transaction.objects.get_or_create(**to_create)
