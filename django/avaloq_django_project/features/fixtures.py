from selenium import webdriver
from behave import fixture, use_fixture
from selenium.webdriver.firefox.options import Options

options = Options()
options.headless = True

@fixture
def browser_firefox(context, timeout=30, **kwargs):
    context.browser = webdriver.Firefox(options=options)
    yield context.browser
    context.browser.quit()


def use_fixture_by_tag(tag, context, fixture_registry):
    fixture_data = fixture_registry.get(tag, None)
    if fixture_data is None:
        raise LookupError("Unknown fixture-tag: %s" % tag)

    fixture_func = fixture_data
    return use_fixture(fixture_func, context)

    fixture_func, fixture_args, fixture_kwargs = fixture_data
    return use_fixture(fixture_func, context, *fixture_args, **fixture_kwargs)