@fixture.browser.firefox
Feature: testing search for invalid data

Scenario: Enter invalid text in search bar and click search
Given the user is on the main page again
When we enter invalid text into the search bar 
And click the search button again
Then no cards are returned

