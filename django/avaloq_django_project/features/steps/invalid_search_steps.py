from behave import given, when, then
from selenium.common.exceptions import NoSuchElementException

@given('the user is on the main page again')
def open_page_invalid_step(context):
    context.browser.get(context.get_url())

@when('we enter invalid text into the search bar')
def enter_invalid_text_step(context):
    context.browser.find_element_by_name("q").send_keys('')

@when('click the search button again')
def execute__invalid_search_step(context):
    context.browser.find_element_by_name("search_button").click()

@then('no cards are returned')
def assert_no_results_step(context):
    container = context.browser.find_element_by_class_name("container")
    try: 
        container.find_element_by_id("cool")
    except NoSuchElementException:
        assert True