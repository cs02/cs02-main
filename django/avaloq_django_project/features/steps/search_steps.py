from behave import given, when, then
import json

@given('the user is on the main page')
def open_page_step(context):
    context.browser.get(context.get_url())

@when('we enter text into the search bar')
def enter_text_step(context):
    context.browser.find_element_by_name("q").send_keys('stock')

@when('click the search button')
def execute_search_step(context):
    context.browser.find_element_by_name("search_button").click()

@then('cards of data should be returned')
def assert_results_step(context):
    container = context.browser.find_element_by_class_name("container")
    cool = container.find_element_by_id("cool")
    div = cool.find_element_by_xpath("//div")
    assert div.find_element_by_class_name("mx-auto")
