@fixture.browser.firefox
Feature: testing search for valid data

Scenario: Enter valid text in search bar and click search
Given the user is on the main page
When we enter text into the search bar 
And click the search button
Then cards of data should be returned

