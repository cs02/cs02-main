from fixtures import browser_firefox, use_fixture_by_tag


fixture_registry = {
    "fixture.browser.firefox": browser_firefox,
}

def before_tag(context, tag):
    if tag.startswith("fixture."):
        return use_fixture_by_tag(tag, context, fixture_registry)

