window.$ = window.jQuery = require('jquery');
require('bootstrap-sass');

import Vue from 'vue';
import sample from "./components/sample.vue";

window.Vue = Vue;
const app = new Vue({
    el: '#app',
    components: {
        sample
    }
});