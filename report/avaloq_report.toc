\contentsline {section}{\numberline {1}Instructions}{1}% 
\contentsline {subsection}{\numberline {1.1}Running the search engine using windows}{1}% 
\contentsline {subsection}{\numberline {1.2}Running the search engine using Linux}{2}% 
\contentsline {subsubsection}{\numberline {1.2.1}Elasticsearch set up}{2}% 
\contentsline {subsubsection}{\numberline {1.2.2}Accessing search engine on linux}{3}% 
\contentsline {subsubsection}{\numberline {1.2.3}Extra docker information}{3}% 
\contentsline {subsection}{\numberline {1.3}Using security model}{4}% 
\contentsline {section}{\numberline {2}Main user types}{5}% 
\contentsline {subsection}{\numberline {2.1}Individual Customer}{5}% 
\contentsline {subsection}{\numberline {2.2}Workplace}{5}% 
\contentsline {subsection}{\numberline {2.3}Bank}{5}% 
\contentsline {section}{\numberline {3}Data Model}{6}% 
\contentsline {section}{\numberline {4}Generation of data}{8}% 
\contentsline {section}{\numberline {5}Brief Discussion of technologies chosen}{9}% 
\contentsline {subsection}{\numberline {5.1}Elasticsearch}{9}% 
\contentsline {subsection}{\numberline {5.2}Django/python/sql}{9}% 
\contentsline {subsection}{\numberline {5.3}VUE}{9}% 
\contentsline {section}{\numberline {6}User Interface}{10}% 
\contentsline {section}{\numberline {7}Access control}{15}% 
\contentsline {subsection}{\numberline {7.1}Django Access Control}{15}% 
\contentsline {subsection}{\numberline {7.2}Elastic Search Permissions}{15}% 
\contentsline {section}{\numberline {8}Project Reflection}{16}% 
\contentsline {section}{\numberline {9}Conclusions}{16}% 
