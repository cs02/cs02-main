from elasticsearch import Elasticsearch

import logging
import requests
from furl import furl


def connect_elasticsearch(host, port):
    es = None
    es = Elasticsearch([{'host': host, 'port': port}])

    if not es.ping():
        raise ConnectionError(
            'Could not connect to ElasticSearch at host {host} and port {port}.'.format(host=host, port=port))

    return es


def create_exchange_rate_index(es):

    settings = {
        "settings": {
            "number_of_shards": 1,
            "number_of_replicas": 0
        },
        "mappings": {
            "properties": {
                "currency_code": {
                    "type": "text"
                },
                "currency_name": {
                    "type": "text"
                },
                "exchange_rate": {
                    "type": "text"
                },
                "bid": {
                    "type": "text"
                },
                "ask": {
                    "type": "text"
                },
                "last_refreshed": {
                    "type": "date",
                    "format": "yyyy-MM-dd HH:mm:ss"
                },
                "timezone": {
                    "type": "text"
                }
            }
        }
    }

    if not es.indices.exists("exchange_rates"):
        es.indices.create(index="exchange_rates", body=settings)

    return True


def get_currency_exchange_data(currency):

    url = furl('https://www.alphavantage.co/query').add({
        'function': 'CURRENCY_EXCHANGE_RATE',
        'from_currency': 'USD',
        'to_currency': currency,
        'apikey': '8Z4V8SPCKXC5Y6SH'})

    r = requests.get(url)
    data = r.json()

    return data


if __name__ == '__main__':
    logging.basicConfig(level=logging.ERROR)

    es = connect_elasticsearch('localhost', 9200)

    es.indices.delete(index='exchange_rates', ignore=[400, 404])

    create_exchange_rate_index(es)

    currencies = ('GBP', 'EUR', 'JPY')

    # Add currency exchange data into ES

    for currency in currencies:
        data = get_currency_exchange_data(currency)

        try:
            data = data['Realtime Currency Exchange Rate']
        except:
            print("API throttled, skipping currency {}".format(currency))
            continue

        to_store = {
            'currency_code': data['3. To_Currency Code'],
            'currency_name': data['4. To_Currency Name'],
            'exchange_rate': data['5. Exchange Rate'],
            'bid': data['8. Bid Price'],
            'ask': data['9. Ask Price'],
            'last_refreshed': data['6. Last Refreshed'],
            'timezone': data['7. Time Zone']
        }

        es.index(index='exchange_rates', id=data['3. To_Currency Code'], body=to_store)

        print("Added currency {}".format(currency))

    es.indices.refresh(index="exchange_rates")

    # Find exchange rates we have saved.
    res = es.search(index="exchange_rates", body={"query": {"match_all": {}}})

    print(res)
